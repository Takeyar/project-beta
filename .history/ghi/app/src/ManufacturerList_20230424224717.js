import React, { useEffect, useState } from 'react';

function ManufacturerList(props) {
    const [manufacturers, setManufacturers] = useState([])
    async function loadManufacturers() {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if (response.ok){
            const data = await response.json();
            setManufacturers(data.manufacturers)
        }
        else {
            console.error(response);
        }
    }
    useEffect (() => {
        loadManufacturers()
    }, []);
    return (
    <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
             <tbody>
          {manufacturers.map((manufacturer, index) => {
            return (
                <td>{manufacturer.name}</td>
        </tbody>
      </table>
    );
    }

}

export default ManufacturerList
