import React, { useEffect, useState } from 'react';

function ManufacturerList(props) {
    const [manufacturers, setManufacturers] = useState([])
    async function loadManufacturers() {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if (response.ok){
            const data = await response.json();
            setManufacturers(data.manufacturers)
        }
        else {
            console.error(response);
        }
    }
    useEffect (() => {
        loadManufacturers()
    }, []);



    return ()

}

export default ManufacturerList
