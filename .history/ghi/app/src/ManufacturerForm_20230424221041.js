import React, { useEffect, useState } from 'react';

function ManufacturerForm (props) {

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {name: name};

        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/'
        const fecthConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(manufacturerUrl, fecthConfig);
        if (response.ok) {
            const newManufacturer = await response.json();
            console.log(newManufacturer);
            setName('');
        }
    }
    const [name, setName] = useState ('');

    const handleName =(event) => {
        const value = event.target.value;
            setName(value);
    }


return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create Manufacturer</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleName} placeholder="Name" required type="text" id="name" name="name" className="form-control" value={name}/>
                <label htmlFor="name">Name</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );

}

export default ManufacturerForm;
