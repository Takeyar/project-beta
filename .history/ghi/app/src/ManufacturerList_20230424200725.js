import React, { useEffect, useState } from 'react';

function ManufacturerList(props) {
    const [manufacturers, setManufacturers] = useState([])
    async function loadManufacturers() {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if (response.ok){
            const data = await response.json();
            setManufacturers(data.manufacturers)
        }
        else {
            console.error(response);
        }
    }
    useEffect (() => {
        loadManufacturers()
    }, []);
    return (
        <table className="table table-striped">
        <thead>
          <tr>
            <th>manufacturer</th>
            <th>Name</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Bin</th>
          </tr>
        </thead>
        <tbody>
            {console.log(props.data)}
            {shoes.map(shoe => {
            return (
                <tr key={shoe.href}>
                    <td>{ shoe.manufactuer }</td>
                    <td>{ shoe.name }</td>
                    <td>{ shoe.color }</td>
                    <td>{ shoe.bin }</td>
                    <td>
                        <img src={ shoe.picture_url } height="100" width="100"/>
                    </td>
                <td></td>
    )

}
