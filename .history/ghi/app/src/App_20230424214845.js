import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import VehicleModelList from './VehicleModelList';




function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers/create" element={<ManufacturerForm />} />
          <Route path="manufacturers/list" element={<ManufacturerList />} />
        <Route path="models" element={<VehicleModelList />}/>
      </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
