import React, { useState, useEffect } from 'react';



function VehicleModelList(props) {
  const [vehicles, setVehicles] = useState([]);

  const handleDelete = async (value) => {
    const vehicleUrl = `http://localhost:8100/api/models/${value.id}/`
    const fetchConfig = {
      method: "delete",
    }
    const response = await fetch(vehicleUrl, fetchConfig);
    if (response.ok) {
      getVehicles();
      console.log("Delete Successful");
    }
  }




  const getVehicles = async () => {
    const url = 'http://localhost:8100/api/models/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setVehicles(data.models);
      console.log('Automobiles', data);
    }
  }


  useEffect(() => {
      getVehicles();
  }, []);



    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
            <th>Delete?</th>
          </tr>
        </thead>
        <tbody>
          {vehicles.map((vehicle, index) => {
            return (
              <tr key={vehicle.id + index}>
                <td>{ vehicle.name }</td>
                <td>{ vehicle.manufacturer.name }</td>
                <td><img src={ vehicle.picture_url } alt={vehicle.name} className="img-thumbnail" height="70" width="70" /></td>
                <td><button onClick={() => handleDelete(vehicle)}>Delete</button></td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  export default VehicleModelList;
