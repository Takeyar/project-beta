import React, { useEffect, useState } from 'react';

function ManufacturerList(props) {
    const [manufacturers, setManufacturers] = useState([])
    async function loadManufacturers() {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if (response.ok){
            const data = await response.json();
            setManufacturers(data.manufacturers)
        }
        else {
            console.error(response);
        }
    }
    useEffect (() => {
        loadManufacturers()
    }, []);
    return (
        <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
            {console.log(props.data)}
            {manufacturers.map(manufacturer => {
            return (
                <tr key={manufacturer.href}>
                    <td>{ manufacturer.name }</td>
                    <td>
                        <img src={ manufacturer.picture_url } height="100" width="100"/>
                    </td>
                <td>
                  <button className="btn btn-danger" onClick={() => {deleteShoe(shoe.href)}}>
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
     );
    )

}
